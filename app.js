var express = require('express');
var app = express();
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./dataBase');
localStorage.setItem("list", JSON.stringify(require("./backUp")));

app.get('/getData', function(req, res) {
  var response = null;
  res.setHeader('Access-Control-Allow-Origin', '*');
  if(req.param('status') == 'All')
    response = JSON.parse(localStorage.getItem("list"));
  else
    response = JSON.parse(localStorage.getItem("list")).filter(function (el) {
      return el.status == req.param('status') ;
    });
  res.json(response);
});
app.get('/deleteData', function(req, res) {
  var response = null;
  var newData = JSON.parse(localStorage.getItem("list")).filter(item => item.id != req.param('id'));
  localStorage.setItem("list",JSON.stringify(newData));
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.json(response);
});

app.listen(process.env.PORT || 4730);
